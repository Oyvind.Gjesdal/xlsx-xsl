<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:arch="http://expath.org/ns/archive"
    xmlns:saxon="http://saxon.sf.net/" xmlns:file="http://expath.org/ns/file"
    xmlns:ssml="http://schemas.openxmlformats.org/spreadsheetml/2006/main"
    xmlns:flub="http://data.ub.uib.no/ns/function-library/" exclude-result-prefixes="xs flub ssml file saxon arch"
   
    version="2.0">
    <xsl:import href="xsl-lib/functions/OOXML/spreadsheetML.xsl"/>
    <!-- stylesheet creating an xml from  xslx file. It copies all individual sheets and 
    returns an xml. Depends on http://expath.org/spec/archive and http://expath.org/spec/file
    extensions which are implemented in Saxon EE and PE since 9.4. 
    Also uses saxon extension function parse-xml to convert the result to processable xml.
    Usable through transformation scenario in oxygen project xml2rdf-
    Go to input - choose dummy.xml select scenario excel to xml.
    Choose input excel document, choose output xml file.
    note:sheets are processed from last to first
    
    Output looks like:
    
            <root>
               <sheet>
               <row><test>dorum</test></row>
                </sheet>
                <sheet>...
                </sheet>
              </root>
    -->
    <xsl:output indent="yes"/>
   
    <xsl:param name="xslx"
        select="'file:/C:/Users/ogj077/prev_ogj077/ogj077/Repos/marcus/flytt/input/Plassering.xlsx'"/>
    <xsl:param name="starting-row" select="'1'"/>   
    <xsl:key name="element-name" match="name" use="@r"/>
   
    <xsl:variable name="missing_title" select="'missing_title'"/>
   
    <!-- lookup table containing id number with its string -->
   
    <xsl:template match="/">
        <xsl:variable name="construct-rows" as="node()+">
            <xsl:sequence select="$workbook-sheets"/>
        </xsl:variable>
        <!--  <xsl:sequence select="$construct-rows"></xsl:sequence>
       -->
      <sheets>
            <xsl:for-each select="$construct-rows">
                <!-- construct one element names tables for each workbook sheet, which is tunneled to subcontext-->
                <xsl:variable name="element-names">
                    <root>
                        <xsl:for-each select="descendant::*:row[@r = $starting-row]/*:c">
                            <xsl:variable name="position" select="position() - 1"/>
                            <xsl:variable name="name"><name r="{replace(@r,'[0-9]+$','')}">
                                <xsl:variable name="current-lookup" select="xs:integer(.)"/>
                                <xsl:sequence
                                    select="flub:cleanXmlName(($shared-strings.xml/key('lookup_by_string_xslx_counter', $current-lookup), '_')[1])"
                                />
                            </name>
                            </xsl:variable>
                            <xsl:message><xsl:sequence select="$name"></xsl:sequence></xsl:message>
                        <xsl:sequence select="$name"></xsl:sequence>
                        </xsl:for-each>
                    </root>
                </xsl:variable>
                <xsl:message><xsl:value-of select="string-join($element-names/descendant-or-self::*:name,' ')"></xsl:value-of></xsl:message>
                <sheet>
                    <xsl:apply-templates mode="xslx">
                        <xsl:with-param name="element-names" tunnel="yes" select="$element-names"/>
                    </xsl:apply-templates>
                </sheet>
            </xsl:for-each>
      </sheets>
        
    </xsl:template>

    <!-- to be ignored-->
    <xsl:template
        match="*:row[xs:integer(@r) &lt;= xs:integer($starting-row)] | *:printOptions | *:pageMargins | *:pageSetup | *:headerFooter"
        mode="xslx"/>


    <xsl:template match="*:row" mode="xslx">
        <xsl:element name="row">
            <xsl:apply-templates mode="xslx"/>
        </xsl:element>
    </xsl:template>

    <xsl:template mode="xslx" match="*">
        <xsl:apply-templates mode="xslx"/>
    </xsl:template>

    <xsl:template match="*:c/*:v" mode="xslx">
        <xsl:param name="element-names" tunnel="yes"/>
        <xsl:variable name="c" select="parent::*:c"/>
          <!--!-->
        <xsl:element
            name="{(key('element-name',replace($c/@r,'[0-9]+',''),$element-names),concat($missing_title,$c/@r))[1]}">
            <xsl:choose>
                <!--lookup string in shared_strings.xml if celltype is 's'-->
                <xsl:when test="matches($c,'#NAME?')">
                    <xsl:message terminate="yes">#NAME? error in xlsx</xsl:message>
                </xsl:when>
                <xsl:when test="$c/@t = 's'">
                    <xsl:variable name="id" select="xs:int(.)"/>
                    <xsl:value-of select="$shared-strings.xml/key('lookup_by_string_xslx_counter', $id)"/>
                </xsl:when>
                <xsl:when test="$c/@s">
                    <xsl:variable name="style" select="key('xf-lookups',xs:integer($c/@s),$styles.xml)"/>
                    
                    <xsl:message><xsl:value-of select="$c/@s"/></xsl:message>
                    <xsl:message select="$styles.xml"/>
                    <xsl:message><xsl:value-of select="$style"/>
                        <xsl:value-of select="$style/@numFmtId"/></xsl:message>
                    <xsl:sequence select="flub:numberFormat(.,xs:integer($style/@numFmtId))"/>
                </xsl:when>              
                <xsl:otherwise>
                    <xsl:apply-templates/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:element>
    </xsl:template>

    <!-- move to xsl-lib in lib for xml? regex from xml specification w3c-->
    <xsl:variable name="NameStartCharBase"
        select="':_a-zA-z&#xC0;-&#xD6;&#xD8;-&#xF6;&#xF8;-&#x2FF;&#x370;-&#x37D;&#x37F;-&#x1FFF;&#x200C;-&#x200D;&#x2070;-&#x218F;&#x2C00;-&#x2FEF;&#x3001;-&#xD7FF;&#xF900;-&#xFDCF;&#xFDF0;-&#xFFFD;&#x10000;-&#xEFFFF;'"/>
    <xsl:variable name="nameStartChar" select="concat('[', $NameStartCharBase, ']')"/>
    <xsl:variable name="nameCharBase"
        select="concat($NameStartCharBase, '\-\.0-9&#xB7;&#x0300;-&#x036F;&#x203F;-&#x2040;')"/>
    <xsl:variable name="nameChar" select="concat('[', $nameCharBase, ']')"/>
    <xsl:variable name="negativeNameChar" select="concat('[^', $nameCharBase, ']')"/>
    <xsl:variable name="name-regex" select="concat('^', $nameStartChar, $nameChar, '*$')"/>

    <xsl:function name="flub:StringisValidXmlName">
        <xsl:param name="string" as="xs:string?"/>
        <xsl:sequence
            select="
                if (matches($string, $name-regex))
                then
                    true()
                else
                    false()"
        />
    </xsl:function>

    <!-- function to create an element name for a string.-->
    <xsl:function name="flub:cleanXmlName">
        <xsl:param name="string"/>
        <xsl:variable name="start-of-xml-fix"
            select="
                if (not(matches($string, concat('^', $nameStartChar)))) then
                    concat('_', $string)
                else
                    $string"/>
        <xsl:variable name="node">
            <root/>
        </xsl:variable>
        <xsl:variable name="element-name"
            select="replace($start-of-xml-fix, $negativeNameChar, '', 'i')"/>
        <xsl:value-of
            select="
                if (string(replace($element-name, '_', ''))) then
                    $element-name
                else
                    generate-id($node)"
        />
    </xsl:function>

   
</xsl:stylesheet>
